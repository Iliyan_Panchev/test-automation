from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, WebDriverException
 

driver = webdriver.Chrome()

USERNAME = 'Test_boy'
PASSWORD = 'Passw0rd!'
try:
    driver.get('https://demoqa.com/login')
except WebDriverException:
    print('Site is offline.')


username_field = driver.find_element(By.CSS_SELECTOR, '#userName')
password_field = driver.find_element(By.CSS_SELECTOR, '#password')

username_field.send_keys(USERNAME)
password_field.send_keys(PASSWORD)
sleep(2)
login_btn = driver.find_element(By.CSS_SELECTOR, '#login').click()
sleep(2)

try:
    delete_acc_btn = driver.find_element(By.CSS_SELECTOR, '.text-center #submit')
    sleep(2)
    delete_acc_btn.click()
    sleep(2)
    driver.find_element(By.ID, 'closeSmallModal-ok').click()
    print('User deleted.') 
except NoSuchElementException:
    print('User not found.')       