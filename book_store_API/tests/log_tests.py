import os
import pytest


@pytest.fixture
def manage_logs(request, autouse=True):
    """Set log file name same as test name"""

    request.config.pluginmanager.get_plugin("logging-plugin")\
        .set_log_path(os.path.join('log', request.node.name + '.log'))