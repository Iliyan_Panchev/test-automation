import datetime
import requests
from requests import Response
from tests.configuration import AUTHORIZE_USER_URL, CREATE_USER_URL, DELETE_BOOK_URL, LIST_ALL_BOOKS_URL, VALID_USER_CREDENTIALS
from tests.fixtures import create_user, user_fixture, reset_add_book_body, reset_update_book_body
from tests.log_tests import manage_logs


                

class TestUser:
    def test_create_new_user_returns_success(self, user_fixture: Response):

        create_user_response, _ = user_fixture
        
        assert create_user_response.status_code == 201
        assert create_user_response.json()['username'] == VALID_USER_CREDENTIALS['userName']
        assert create_user_response.json()['books'] == []

    def test_create_user_when_user_exists_returns_not_acceptable(self, user_fixture):
        # create_user_response, _ = user_fixture

        api_response = create_user(VALID_USER_CREDENTIALS)

        assert api_response.status_code == 406
        assert api_response.json()['code'] == '1204'
        assert api_response.json()['message'] == 'User exists!'



    def test_generate_token_returns_success(self, user_fixture: Response):
        _ , create_token_response = user_fixture

        assert create_token_response.status_code == 200
        assert create_token_response.json()['status'] == 'Success'
        assert datetime.datetime.strptime(create_token_response.json()['expires'], '%Y-%m-%dT%H:%M:%S.%fZ') > datetime.datetime.now()
        assert create_token_response.json()['result'] == 'User authorized successfully.'


    def test_authorized_returns_true_when_right_credentials(self, user_fixture: Response):
       
        api_response = requests.post(AUTHORIZE_USER_URL, json=VALID_USER_CREDENTIALS)

        assert api_response.status_code == 200
        assert api_response.text == 'true'


    def test_delete_user_returns_no_content_when_deleted(self, user_fixture: Response):
        user_response, create_token_response = user_fixture
        user_id = user_response.json()['userID']
        token = create_token_response.json()['token']

        DELETE_USER_URL = f'{CREATE_USER_URL}/{user_id}'

        api_response = requests.delete(DELETE_USER_URL, headers={"Authorization": f"Bearer {token}"})

        assert api_response.status_code == 204
 
    
    def test_get_user_by_id_returns_success(self, user_fixture: Response):
        user_response, create_token_response = user_fixture
        token = create_token_response.json()['token']
        user_id = user_response.json()['userID']
        get_user_response = requests.get(CREATE_USER_URL + '/' + user_id, headers={"Authorization": f"Bearer {token}"})

        assert get_user_response.status_code == 200


    def test_get_user_by_id_returns_not_authorized(self, user_fixture: Response):
        user_response, _ = user_fixture
        user_id = user_response.json()['userID']
        get_user_response = requests.get(CREATE_USER_URL + '/' + user_id)
        
        assert get_user_response.status_code == 401
        assert get_user_response.json()['code'] == '1200'
        assert get_user_response.json()['message'] == 'User not authorized!'


class TestBookStore:
    def test_add_book_to_collection_returns_created(self, user_fixture, reset_add_book_body):
        book_to_be_added = requests.get(LIST_ALL_BOOKS_URL).json()['books'][0]['isbn']

        user_response, token_response = user_fixture
        token = token_response.json()['token']
        ADD_BOOK_BODY = reset_add_book_body
        ADD_BOOK_BODY['userId'] = user_response.json()['userID']
        ADD_BOOK_BODY['collectionOfIsbns'].append({'isbn': book_to_be_added})

        book_added_response = requests.post(LIST_ALL_BOOKS_URL, json=ADD_BOOK_BODY, headers={"Authorization": f"Bearer {token}"})

        assert book_added_response.status_code == 201
        assert book_added_response.json()['books'][0]['isbn'] == book_to_be_added # validate that the book we added is in the user's collection


    def test_add_book_to_collection_returns_bad_request(self, user_fixture, reset_add_book_body):
        new_book = 'NONEXISTENT ISBN'

        user_response, token_response = user_fixture
        token = token_response.json()['token']
        ADD_BOOK_BODY = reset_add_book_body
        ADD_BOOK_BODY['userId'] = user_response.json()['userID']
        ADD_BOOK_BODY['collectionOfIsbns'].append({'isbn': new_book})

        book_added_response = requests.post(LIST_ALL_BOOKS_URL, json=ADD_BOOK_BODY, headers={"Authorization": f"Bearer {token}"})

        assert book_added_response.status_code == 400
        assert book_added_response.json()['code'] == '1205'
        assert book_added_response.json()['message'] == 'ISBN supplied is not available in Books Collection!'

    
    def test_add_book_to_collection_returns_unauthorized(self, user_fixture, reset_add_book_body):
        new_book = requests.get(LIST_ALL_BOOKS_URL).json()['books'][0]['isbn']

        user_response, _ = user_fixture
        ADD_BOOK_BODY = reset_add_book_body
        ADD_BOOK_BODY['userId'] = user_response.json()['userID']
        ADD_BOOK_BODY['collectionOfIsbns'].append({'isbn': new_book})

        book_added_response = requests.post(LIST_ALL_BOOKS_URL, json=ADD_BOOK_BODY)

        assert book_added_response.status_code == 401
        assert book_added_response.json()['code'] == '1200'
        assert book_added_response.json()['message'] == 'User not authorized!'


    def test_update_book_collection_returns_success(self, user_fixture, reset_add_book_body, reset_update_book_body):
        old_book_isbn = requests.get(LIST_ALL_BOOKS_URL).json()['books'][0]['isbn']
        new_book_isbn = requests.get(LIST_ALL_BOOKS_URL).json()['books'][1]['isbn']

        user_response, token_response = user_fixture
        user_id = user_response.json()['userID']
        token = token_response.json()['token']

        ADD_BOOK_BODY = reset_add_book_body
        ADD_BOOK_BODY['userId'] = user_response.json()['userID']
        ADD_BOOK_BODY['collectionOfIsbns'].append({'isbn': old_book_isbn})

        book_added_response = requests.post(LIST_ALL_BOOKS_URL, json=ADD_BOOK_BODY, headers={"Authorization": f"Bearer {token}"})
        assert book_added_response.json()['books'][0]['isbn'] == old_book_isbn
        assert book_added_response.status_code == 201

        UDPATE_BOOK_BODY = reset_update_book_body
        UDPATE_BOOK_BODY['isbn'] = new_book_isbn
        UDPATE_BOOK_BODY['userId'] = user_id

        book_updated_response = requests.put(LIST_ALL_BOOKS_URL + '/' + old_book_isbn, json=UDPATE_BOOK_BODY, headers={"Authorization": f"Bearer {token}"})
        
        assert book_updated_response.status_code == 200
        assert book_updated_response.json()['books'][0]['isbn'] == new_book_isbn #validate that the updated book is in user's collection
        

    def test_book_with_isbn_has_pages(self):

        book_list = requests.get(LIST_ALL_BOOKS_URL).json()['books']
        
        assert any(book['isbn'] == '9781491904244' and book['pages'] == 278 for book in book_list)
    

    def test_remove_added_book_returns_no_content(self, user_fixture, reset_add_book_body, reset_update_book_body):
        new_book_isbn = requests.get(LIST_ALL_BOOKS_URL).json()['books'][0]['isbn']

        user_response, token_response = user_fixture
        token = token_response.json()['token']
        user_id = user_response.json()['userID']

        ADD_BOOK_BODY = reset_add_book_body
        ADD_BOOK_BODY['userId'] = user_id
        ADD_BOOK_BODY['collectionOfIsbns'].append({'isbn': new_book_isbn})

        book_added_response = requests.post(LIST_ALL_BOOKS_URL, json=ADD_BOOK_BODY, headers={"Authorization": f"Bearer {token}"})

        assert book_added_response.status_code == 201
        assert book_added_response.json()['books'][0]['isbn'] == new_book_isbn #validated that book has been added

        UDPATE_BOOK_BODY = reset_update_book_body
        UDPATE_BOOK_BODY['userId'] = user_id
        UDPATE_BOOK_BODY['isbn'] = new_book_isbn

        book_deleted_response = requests.delete(DELETE_BOOK_URL, json=UDPATE_BOOK_BODY, headers={"Authorization": f"Bearer {token}"})

        assert book_deleted_response.status_code == 204

        user_collection_response = requests.get(CREATE_USER_URL + '/' + user_id, headers={"Authorization": f"Bearer {token}"})

        assert user_collection_response.json()['books'] == [] #validate that book has been removed


    def test_remove_nonexistent_book_returns_bad_request(self, user_fixture, reset_update_book_body):
        user_response, token_response = user_fixture
        token = token_response.json()['token']
        user_id = user_response.json()['userID']

        UDPATE_BOOK_BODY = reset_update_book_body
        UDPATE_BOOK_BODY['userId'] = user_id
        UDPATE_BOOK_BODY['isbn'] = 'NONEXISTENT ISBN'

        book_deleted_response = requests.delete(DELETE_BOOK_URL, json=UDPATE_BOOK_BODY, headers={"Authorization": f"Bearer {token}"})

        assert book_deleted_response.status_code == 400
        assert book_deleted_response.json()['code'] == '1206'
        assert book_deleted_response.json()['message'] == "ISBN supplied is not available in User's Collection!"
