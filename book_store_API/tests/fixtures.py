import requests
from requests import Response
import pytest

from tests.configuration import CREATE_USER_URL, GENERATE_TOKEN_URL, VALID_USER_CREDENTIALS


def create_user(credentials: dict):
    return requests.post(CREATE_USER_URL, json=credentials)

def delete_user(user_id, token):
    return requests.delete(CREATE_USER_URL + '/' + user_id, 
                           headers={"Authorization": f"Bearer {token}"})

def get_token(credentials: dict):
    return requests.post(GENERATE_TOKEN_URL, json=credentials)


@pytest.fixture(scope="function")
def user_fixture() -> (Response, Response):
    create_user_response = create_user(VALID_USER_CREDENTIALS)
    generate_token_response = get_token(VALID_USER_CREDENTIALS)

    yield create_user_response, generate_token_response

    delete_user(create_user_response.json()['userID'],
                 generate_token_response.json()['token'])


@pytest.fixture(scope="function")
def reset_add_book_body():
    return {
    "userId": "string",
    "collectionOfIsbns": []
    }


@pytest.fixture(scope='function')
def reset_update_book_body():
    return {
    "userId": "string",
    "isbn": "string"
    }