from dotenv import load_dotenv
import os

load_dotenv()

GENERATE_TOKEN_URL = 'https://demoqa.com/Account/v1/GenerateToken'
CREATE_USER_URL = "https://demoqa.com/Account/v1/User"
AUTHORIZE_USER_URL = 'https://demoqa.com/Account/v1/Authorized'
LIST_ALL_BOOKS_URL = 'https://demoqa.com/BookStore/v1/Books'
DELETE_BOOK_URL = 'https://demoqa.com/BookStore/v1/Book'

VALID_TEST_USERNAME = 'Test_boy'
VALID_TEST_PASSWORD = '1234ABC!z'


VALID_USER_CREDENTIALS = {
    "userName": VALID_TEST_USERNAME, 
    "password": VALID_TEST_PASSWORD
    }

INVALID_USER_CREDENTIALS = {
    "userName": VALID_TEST_USERNAME,
    "password": VALID_TEST_PASSWORD
}



ADD_BOOK_BODY = {
  "userId": "string",
  "collectionOfIsbns": []
}


UDPATE_BOOK_BODY = {
  "userId": "string",
  "isbn": "string"
}